import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Connection, ConnectionOptions, createConnection, getConnection } from 'typeorm';
import { Workout } from '../workout/workout';
import { Exercise } from '../workout/exercise/exercise';

@Injectable({
    providedIn: 'root',
})
export class DatabaseService {

    constructor(private platform: Platform) {
    }

    async initDatabase(): Promise<void> {
        try {
            await getConnection();
        } catch (e) {
            await this.setupConnection();
        }
    }

    private async setupConnection(): Promise<Connection> {
        let options: ConnectionOptions;

        if (this.platform.is('cordova')) {
            options = {
                type: 'cordova',
                database: 'workout-track',
                location: 'default',
            };
        } else {
            options = {
                type: 'sqljs',
                location: 'browser',
                autoSave: true,
            };
        }

        return createConnection({
            ...options,
            synchronize: true,
            entities: [
                Workout,
                Exercise,
            ],
        });
    }

}
