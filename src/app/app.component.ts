import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DatabaseService } from './data/database.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {

    initialized = false;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private database: DatabaseService,
    ) {
        this.initializeApp();
    }

    async initializeApp() {
        await this.platform.ready();
        this.statusBar.styleLightContent();
        this.splashScreen.hide();
        await this.database.initDatabase();
        this.initialized = true;
    }
}
