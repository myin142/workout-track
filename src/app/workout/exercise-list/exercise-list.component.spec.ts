import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExerciseListComponent } from './exercise-list.component';
import { By } from '@angular/platform-browser';

describe('ExerciseListComponent', () => {
    let component: ExerciseListComponent;
    let fixture: ComponentFixture<ExerciseListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ExerciseListComponent],
            imports: [IonicModule.forRoot()],
        }).compileComponents();

        fixture = TestBed.createComponent(ExerciseListComponent);
        component = fixture.componentInstance;
    }));

    it('should list exercises sorted by id', () => {
        component.exercises = [
            {name: 'Exercise 2', id: 2},
            {name: 'Exercise 1', id: 1},
        ] as any;
        fixture.detectChanges();

        const items = fixture.debugElement.queryAll(By.css('ion-item'));
        expect(items.length).toEqual(2);
        expect(items[0].nativeElement.innerHTML).toContain('Exercise 1');
        expect(items[1].nativeElement.innerHTML).toContain('Exercise 2');
    });

    it('should show delete icon if delete true', () => {
        component.exercises = [{name: 'Exercise'}] as any;
        component.editable = true;
        fixture.detectChanges();

        expect(fixture.debugElement.query(By.css('.delete-icon'))) .toBeTruthy();
    });

    it('should emit remove if delete icon clicked', () => {
        spyOn(component.remove, 'emit');
        component.exercises = [{name: 'Exercise'}] as any;
        component.editable = true;
        fixture.detectChanges();

        fixture.debugElement.query(By.css('.delete-icon')).nativeElement.click();
        expect(component.remove.emit).toHaveBeenCalledWith({name: 'Exercise'});
    });
});
