import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Workout } from '../workout';

@Entity('exercise')
export class Exercise {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public name: string;

    @ManyToOne(type => Workout, workout => workout.exercises, {onDelete: 'CASCADE'})
    public workouts: Workout;

}
