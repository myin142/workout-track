#!/bin/bash

if [[ -z $1 ]]; then
	echo "Missing keystore file";
	exit 1;
fi

if [[ -z $2 ]]; then
	echo "Missing keystore password";
	exit 1;
fi

KEYSTORE="$1"
KEYSTORE_PW="$2"

APK="${3:-output/android-release-unsigned.apk}"

jarsigner -verbose \
	-sigalg SHA1withRSA \
	-digestalg SHA1 \
	-keystore "$KEYSTORE.keystore" \
	-storepass "$KEYSTORE_PW" \
	"$APK" \
    "$KEYSTORE"

jarsigner --verify "$APK"
"${ANDROID_HOME}"/build-tools/"${ANDROID_VERSION}"/zipalign -v 4 "$APK" output/android-release.apk
