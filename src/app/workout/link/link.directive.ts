import { Directive, HostListener, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

/**
 * Angular Router Link State does not work with other elements except <a>
 */
@Directive({
    selector: '[appLink]',
})
export class LinkDirective {

    @Input('appLink') link: string;
    @Input() state: any;

    constructor(private route: ActivatedRoute,
                private router: Router) {
    }

    @HostListener('click')
    navigate(): void {
        this.router.navigate([this.link], {relativeTo: this.route, state: this.state});
    }

}
