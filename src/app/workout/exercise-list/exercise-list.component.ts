import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Exercise } from '../exercise/exercise';
import { moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
    selector: 'app-exercise-list',
    templateUrl: './exercise-list.component.html',
    styleUrls: ['./exercise-list.component.scss'],
})
export class ExerciseListComponent {
    @Input() exercises: Exercise[];
    @Input() editable = false;
    @Output() remove = new EventEmitter<Exercise>();

    get sortedExercises(): Exercise[] {
        return this.exercises.sort((e1, e2) => e1.id - e2.id);
    }

    reorder(ev: CustomEvent): void {
        moveItemInArray(this.exercises, ev.detail.from, ev.detail.to);
        ev.detail.complete();
    }
}
