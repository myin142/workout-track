import { TestBed, inject } from '@angular/core/testing';

import { SessionGuard } from './session.guard';
import { IonicModule } from '@ionic/angular';
import { SessionComponent } from './session.component';

describe('SessionGuardGuard', () => {

    let mockSessionComponent;

    beforeEach(() => {
        mockSessionComponent = jasmine.createSpyObj('SessionComponent', ['cleanSession']);

        TestBed.configureTestingModule({
            imports: [IonicModule.forRoot()],
            providers: [SessionGuard],
        });
    });

    it('should return true if clean session', inject([SessionGuard], (guard: SessionGuard) => {
        mockSessionComponent.cleanSession.and.returnValue(true);
        expect(guard.canDeactivate(mockSessionComponent, null, null)).toEqual(true);
    }));

/*
    it('should resolve true if clicked on leave', inject([SessionGuard], async (guard: SessionGuard) => {
        mockSessionComponent.cleanSession.and.returnValue(false);
        const canLeave = await guard.canDeactivate(mockSessionComponent, null, null);
        expect(canLeave).toBeTruthy();
    }));
*/
});
