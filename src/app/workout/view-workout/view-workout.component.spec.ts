import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewWorkoutComponent } from './view-workout.component';
import { Location } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('ViewWorkoutComponent', () => {
    let component: ViewWorkoutComponent;
    let fixture: ComponentFixture<ViewWorkoutComponent>;

    let location;

    beforeEach(async(() => {
        location = jasmine.createSpyObj('Location', ['getState']);

        TestBed.configureTestingModule({
            declarations: [ViewWorkoutComponent],
            imports: [IonicModule.forRoot()],
            providers: [
                {provide: Location, useValue: location},
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();

        fixture = TestBed.createComponent(ViewWorkoutComponent);
        component = fixture.componentInstance;
    }));

    it('should load workout from state', () => {
        location.getState.and.returnValue({workout: {}});
        expect(component.workout).toEqual({} as any);
    });
});
