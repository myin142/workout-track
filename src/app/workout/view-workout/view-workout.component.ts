import { Component } from '@angular/core';
import { Workout } from '../workout';
import { Location } from '@angular/common';

@Component({
    selector: 'app-view-workout-session',
    templateUrl: './view-workout.component.html',
    styleUrls: ['./view-workout.component.scss'],
})
export class ViewWorkoutComponent {

    constructor(private location: Location) {
    }

    get workout(): Workout {
        return this.state.workout;
    }

    private get state(): any {
        return this.location.getState();
    }

}
