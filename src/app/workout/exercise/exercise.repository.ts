import { Injectable } from '@angular/core';
import { Exercise } from './exercise';
import { BaseRepository } from '../../data/base.repository';

@Injectable()
export class ExerciseRepository extends BaseRepository<Exercise> {
    tableName = 'exercise';
}
