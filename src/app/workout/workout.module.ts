import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { WorkoutPage } from './workout.page';
import { WorkoutRepository } from './workout.repository';
import { ExerciseRepository } from './exercise/exercise.repository';
import { WorkoutSessionComponent } from './workout-session/workout-session.component';
import { ViewWorkoutComponent } from './view-workout/view-workout.component';
import { ExerciseListComponent } from './exercise-list/exercise-list.component';
import { LinkDirective } from './link/link.directive';
import { SessionGuard } from './session-guard/session.guard';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([
            {path: '', component: WorkoutPage},
            {path: 'session', component: WorkoutSessionComponent, canDeactivate: [SessionGuard]},
            {path: 'view', component: ViewWorkoutComponent},
        ]),
    ],
    declarations: [
        WorkoutPage,
        WorkoutSessionComponent,
        ViewWorkoutComponent,
        ExerciseListComponent,
        LinkDirective,
    ],
    providers: [
        WorkoutRepository,
        ExerciseRepository,
        SessionGuard,
    ],
})
export class WorkoutModule {
}
