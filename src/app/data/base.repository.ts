import { getRepository, Repository } from 'typeorm';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

export abstract class BaseRepository<T> {

    abstract tableName: string;
    private changes = new BehaviorSubject(null);

    get repository(): Repository<T> {
        return getRepository(this.tableName);
    }

    async save(...entity: T[]): Promise<T> {
        return this.repository.save(entity).then(this.notifyChange.bind(this));
    }

    async delete(entity: T): Promise<T> {
        return this.repository.remove(entity).then(this.notifyChange.bind(this));
    }

    private notifyChange(x: any): any {
        this.changes.next(null);
        return x;
    }

    async findAll(): Promise<T[]> {
        return this.repository.find();
    }

    watchValues(): Observable<T[]> {
        return this.changes.pipe(
            switchMap(() => from(this.findAll())),
        );
    }

}
