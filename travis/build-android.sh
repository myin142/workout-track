
#!/bin/bash -v

set -e

# Build Ionic App for Android
ionic cordova platform add android --nofetch
# --prod breaks the app, does not work as intended anymore
ionic cordova build android --release
