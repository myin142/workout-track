import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WorkoutPage } from './workout.page';
import { WorkoutRepository } from './workout.repository';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('WorkoutPage', () => {
    let component: WorkoutPage;
    let fixture: ComponentFixture<WorkoutPage>;

    let workoutRepo;

    beforeEach(async(() => {
        workoutRepo = jasmine.createSpyObj('WorkoutRepository', ['watchValues', 'delete']);

        TestBed.configureTestingModule({
            declarations: [WorkoutPage],
            imports: [
                IonicModule.forRoot(),
                RouterTestingModule,
            ],
            providers: [
                {provide: WorkoutRepository, useValue: workoutRepo},
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();

        fixture = TestBed.createComponent(WorkoutPage);
        component = fixture.componentInstance;
    }));

    it('should list workouts', () => {
        workoutRepo.watchValues.and.returnValue(of([{}, {}]));
        fixture.detectChanges();

        const workouts = fixture.debugElement.queryAll(By.css('ion-item'));
        expect(workouts.length).toEqual(2);
    });
});
