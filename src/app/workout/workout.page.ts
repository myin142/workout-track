import { Component, OnInit } from '@angular/core';
import { WorkoutRepository } from './workout.repository';
import { Workout } from './workout';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Component({
    selector: 'app-workout-page',
    templateUrl: 'workout.page.html',
    styleUrls: ['workout.page.scss'],
})
export class WorkoutPage implements OnInit {

    workouts: Observable<Workout[]>;

    constructor(private workoutRepo: WorkoutRepository) {
    }

    ngOnInit(): void {
        this.workouts = this.workoutRepo.watchValues().pipe(shareReplay());
    }

    deleteWorkout(workout: Workout): void {
        this.workoutRepo.delete(workout);
    }

}
