import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import { SessionComponent } from './session.component';
import { AlertController } from '@ionic/angular';

@Injectable()
export class SessionGuard implements CanDeactivate<SessionComponent> {

    constructor(private alertController: AlertController) {
    }

    canDeactivate(component: SessionComponent,
                  currentRoute: ActivatedRouteSnapshot,
                  currentState: RouterStateSnapshot,
                  nextState?: RouterStateSnapshot): Promise<boolean | UrlTree> | boolean {
        if (component.cleanSession()) {
            return true;
        }

        return new Promise(async resolve => {
            const alert = await this.alertController.create({
                header: 'Session not saved',
                message: 'Are you sure you want to leave the page?',
                buttons: [
                    {text: 'Leave', handler: () => resolve(true)},
                    {text: 'Stay', handler: () => resolve(false)},
                ],
            });
            await alert.present();
        });
    }
}
