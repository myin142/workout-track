import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Exercise } from './exercise/exercise';

@Entity('workout')
export class Workout {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column({nullable: true})
    public finishDate: Date;

    @OneToMany(type => Exercise, exercise => exercise.workouts, {
        cascade: true,
        eager: true,
    })
    public exercises: Exercise[];

}
