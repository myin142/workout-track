import { Injectable } from '@angular/core';
import { Workout } from './workout';
import { BaseRepository } from '../data/base.repository';

@Injectable()
export class WorkoutRepository extends BaseRepository<Workout> {
    tableName = 'workout';

    async findAll(): Promise<Workout[]> {
        return this.repository.find({order: {finishDate: 'DESC'}});
    }
}
