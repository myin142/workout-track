import { Component, OnInit } from '@angular/core';
import { WorkoutRepository } from '../workout.repository';
import { Exercise } from '../exercise/exercise';
import { Workout } from '../workout';
import { SessionComponent } from '../session-guard/session.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
    selector: 'app-workout-session',
    templateUrl: './workout-session.component.html',
    styleUrls: ['./workout-session.component.scss'],
})
export class WorkoutSessionComponent implements OnInit, SessionComponent {

    private SAVED_SESSION_WORKOUT = 'saved-session-workout';
    workout: Workout;
    exerciseInput = '';

    constructor(private workoutRepo: WorkoutRepository,
                private storage: Storage,
                private router: Router,
                private route: ActivatedRoute) {
    }

    async ngOnInit() {
        const prevWorkout = await this.storage.get(this.SAVED_SESSION_WORKOUT);

        this.workout = prevWorkout || new Workout();
        if (this.workout.exercises == null) {
            this.workout.exercises = [];
        }
    }

    addExercise(): void {
        if (this.exerciseInput != null && this.exerciseInput.trim() !== '') {
            const exercise = new Exercise();
            exercise.name = this.exerciseInput;

            if (this.workout.exercises.findIndex(x => this.isEqualExercise(x, exercise)) === -1) {
                this.workout.exercises.push(exercise);
            }

            this.exerciseInput = '';
            this.storage.set(this.SAVED_SESSION_WORKOUT, this.workout);
        }
    }

    private isEqualExercise(exercise1: Exercise, exercise2: Exercise): boolean {
        return exercise1.name === exercise2.name;
    }

    removeExercise(exercise: Exercise): void {
        this.workout.exercises = this.workout.exercises.filter(x => !this.isEqualExercise(x, exercise));
    }

    async finishWorkout() {
        if (this.workout.exercises.length > 0) {
            this.workout.finishDate = new Date();
            await this.workoutRepo.save(this.workout);
            this.workout.exercises = [];
            this.storage.set(this.SAVED_SESSION_WORKOUT, null);
            this.router.navigate(['../'], {relativeTo: this.route});
        }
    }

    cleanSession(): boolean {
        return this.workout.exercises.length === 0;
    }

}
