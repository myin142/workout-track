import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WorkoutSessionComponent } from './workout-session.component';
import { FormsModule } from '@angular/forms';
import { WorkoutRepository } from '../workout.repository';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Workout } from '../workout';

describe('WorkoutSessionComponent', () => {
    let component: WorkoutSessionComponent;
    let fixture: ComponentFixture<WorkoutSessionComponent>;

    let workoutRepo, storage;

    beforeEach(async(() => {
        workoutRepo = jasmine.createSpyObj('WorkoutRepository', ['save']);
        storage = jasmine.createSpyObj('Storage', ['get', 'set']);

        TestBed.configureTestingModule({
            declarations: [WorkoutSessionComponent],
            imports: [
                IonicModule.forRoot(),
                FormsModule,
                RouterTestingModule,
            ],
            providers: [
                { provide: WorkoutRepository, useValue: workoutRepo },
                { provide: Storage, useValue: storage },
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        }).compileComponents();

        fixture = TestBed.createComponent(WorkoutSessionComponent);
        component = fixture.componentInstance;
        storage.get.and.returnValue(Promise.resolve(null));
    }));

    it('should load saved workout session', fakeAsync(() => {
        storage.get.and.returnValue(Promise.resolve({exercises: ['1', '2']}));
        fixture.detectChanges();

        tick();
        expect(component.workout).toEqual({exercises: ['1', '2']} as any);
    }));

    describe('Add Exercise', () => {

        beforeEach(async(() => {
            fixture.detectChanges();
        }));

        it('should add current exercise input to workout exercises', () => {
            component.exerciseInput = 'Exercise';
            component.addExercise();

            expect(component.workout.exercises).toEqual(jasmine.arrayContaining([
                jasmine.objectContaining({ name: 'Exercise' }),
            ]));
        });

        it('should not add exercise input if name already exists', () => {
            component.workout.exercises = [{ name: 'Exercise' }] as any;
            component.exerciseInput = 'Exercise';
            component.addExercise();

            expect(component.workout.exercises).toEqual(jasmine.arrayWithExactContents([
                jasmine.objectContaining({ name: 'Exercise' }),
            ]));
        });

        it('should not add empty exercise input', () => {
            component.workout.exercises = [];
            component.exerciseInput = '';
            component.addExercise();

            expect(component.workout.exercises).toEqual([]);
        });

        it('should not add exercise input with only spaces', () => {
            component.workout.exercises = [];
            component.exerciseInput = '   ';
            component.addExercise();

            expect(component.workout.exercises).toEqual([]);
        });

        it('should reset exercise input after adding', () => {
            component.exerciseInput = 'Exercise';
            component.addExercise();
            expect(component.exerciseInput).toBeFalsy();
        });

        it('should save session', () => {
            component.exerciseInput = 'Exercise';
            component.addExercise();
            expect(storage.set).toHaveBeenCalledWith(jasmine.anything(), component.workout);
        });

    });

    describe('Remove Exercise', () => {

        beforeEach(async(() => {
            fixture.detectChanges();
        }));

        it('should remove exercise from workout exercises', () => {
            component.workout.exercises = [{ name: 'Exercise' }] as any;
            component.removeExercise({ name: 'Exercise' } as any);
            expect(component.workout.exercises).toEqual([]);
        });

    });

    describe('Finish Workout', () => {

        beforeEach(async(() => {
            fixture.detectChanges();
        }));

        it('should save workout if there are exercises', () => {
            component.workout.exercises = [null];
            component.finishWorkout();

            expect(workoutRepo.save).toHaveBeenCalledWith(component.workout);
        });

        it('should not save workout if there are no exercises', () => {
            component.workout.exercises = [];
            component.finishWorkout();

            expect(workoutRepo.save).not.toHaveBeenCalled();
        });

        it('should set finished date to current date', () => {
            jasmine.clock().mockDate(new Date(2020, 1, 1));
            component.workout.exercises = [null];

            component.finishWorkout();

            expect(workoutRepo.save).toHaveBeenCalledWith(jasmine.objectContaining({
                finishDate: new Date(2020, 1, 1),
            }));
        });

        it('should clean session', fakeAsync(() => {
            component.workout.exercises = [null, null];
            component.finishWorkout();
            tick();

            expect(component.cleanSession()).toBeTruthy();
        }));

        it('should clear saved session', fakeAsync(() => {
            component.workout.exercises = [null];
            component.finishWorkout();
            tick();

            expect(storage.set).toHaveBeenCalledWith(jasmine.anything(), null);
        }));

    });
});
